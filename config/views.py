from django.shortcuts import render, redirect, get_object_or_404
from django.http import Http404, HttpResponse
from django.views.generic.base import TemplateView

class HomePageView(TemplateView):
    template_name = 'index.html'

class NewsPageView(TemplateView):
    template_name = 'news.html'

class ContactPageView(TemplateView):
    template_name = 'contact.html'