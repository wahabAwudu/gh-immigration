from django.conf.urls import include, url
from django.conf.urls.static import static
from django.conf import settings
from django.contrib import admin

from rest_framework import routers
# apps API Viewsets imports
from gis.apps.serials.api.viewsets import SerialNumberAPIViewset
from gis.apps.deadlines.api.viewsets import DeadlineAPIViewset
from gis.apps.applications.api.viewsets import ApplicationAPIViewset

router = routers.DefaultRouter()
# api router url confs
router.register('serial_numbers', SerialNumberAPIViewset, base_name='serial_number')
router.register('applications', ApplicationAPIViewset, base_name='application')
router.register('deadlines', DeadlineAPIViewset, base_name='deadline')

# app imports
from .views import HomePageView, NewsPageView, ContactPageView

urlpatterns = [
    # static pages
    url(r'^$', HomePageView.as_view(), name='home'),
    url(r'^contact/', ContactPageView.as_view(), name='contact'),
    url(r'^news/', NewsPageView.as_view(), name='news'),
    # app url confs
    url(r'^serial/', include('gis.apps.serials.urls', namespace='serials')),
    url(r'^application/', include('gis.apps.applications.urls', namespace='apply')),
    url(r'^deadline/', include('gis.apps.deadlines.urls', namespace='deadline')),

    # api
    url(r'^api/', include(router.urls, namespace='api')),
    # admin
    url(r'^admin/', admin.site.urls),
    # authentication
    url(r'^accounts/', include('registration.backends.default.urls')),
]

# if settings.DEBUG:
#     urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
#     urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
