# gh-immigration

### What is this repository for? ###

 it is a project designed to enable Ghana Imigration Service take applications and manage them through a database.
 
 users are supposed to signup and enter a serial number (which they would have bought from an outlet). this will allow them to access the information they enter into the system in order to update the info provided for a number of times (three[3] times according to our system).
 
 it was with django 1.11.8 version.
 and basic HTML, css and Js alongside bootstrap.

### How do I get set up? ###

*clone the repostitory.
*create a postgresql database named 'gisdb'
*create a django virtual environment and activate it.

* go into the main project directory and run pip install -r requirements/local.txt
to install the requirements of the project
* create a super user account by running 'python manage.py createsuperuser' in the terminal and follow the prompt
* run migrations with 'python manage.py migrate'
* start the development server with 'python manage.py runserver'
