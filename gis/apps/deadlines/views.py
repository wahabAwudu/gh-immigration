from django.shortcuts import render, redirect, get_object_or_404
from django.http import Http404, HttpResponse, HttpResponseRedirect
from django.core.urlresolvers import reverse_lazy
from django.views.generic import UpdateView, CreateView, ListView, DeleteView
from datetime import date

# from this App (i.e the Serials App)
from .models import DeadlineDate


from braces.views import LoginRequiredMixin, StaffuserRequiredMixin, SuperuserRequiredMixin

class DeadlineListView(LoginRequiredMixin, StaffuserRequiredMixin, ListView):
    model = DeadlineDate
    queryset = DeadlineDate.objects.all().order_by('-timestamp')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["page_header"] = "List of INACTIVE Serial Numbers"
        return context


class DeadlineCreateView(LoginRequiredMixin, StaffuserRequiredMixin, CreateView):
    model = DeadlineDate
    fields = ['deadline']
    success_url = reverse_lazy("deadline:list")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["page_title"] = "Create Deadline" 
        context["page_header"] = "create Deadline"
        context["button_text"] = "Create"
        return context
    

class DeadlineUpdateView(LoginRequiredMixin, StaffuserRequiredMixin, UpdateView):
    model = DeadlineDate
    fields = ['deadline']
    success_url = reverse_lazy("deadline:list")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["page_title"] = "Update Deadline" 
        context["page_header"] = "Update Deadline"
        context["button_text"] = "Update"
        return context


class DeadlineDeleteView(LoginRequiredMixin, SuperuserRequiredMixin,DeleteView):
    model = DeadlineDate
    success_url = reverse_lazy("deadline:list")
