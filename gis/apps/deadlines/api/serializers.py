from rest_framework.serializers import (
    ModelSerializer,
    CurrentUserDefault,
    PrimaryKeyRelatedField,
    SerializerMethodField,
    HyperlinkedIdentityField
)
from gis.apps.deadlines.models import DeadlineDate


class DeadlineAPISerializer(ModelSerializer):
    user = PrimaryKeyRelatedField(read_only=True, default=CurrentUserDefault())
    id = SerializerMethodField()

    class Meta:
        model = DeadlineDate
        fields = [
            'id',
            'user',
            'timestamp',
            'deadline'
        ]

    def get_id(self, obj):
        return obj.id
