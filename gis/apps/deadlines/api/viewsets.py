from django.template.loader import get_template
from django.template import context
from django.contrib.auth.models import User

from rest_framework.viewsets import ModelViewSet
from rest_framework.permissions import IsAdminUser, IsAuthenticated, AllowAny
from rest_framework.response import Response
from rest_framework import status

from .serializers import DeadlineAPISerializer
from gis.apps.deadlines.models import DeadlineDate

class DeadlineAPIViewset(ModelViewSet):
    model = DeadlineDate
    serializer_class = DeadlineAPISerializer
    permission_classes = [AllowAny]

    def get_queryset(self):
        return DeadlineDate.objects.all().order_by('-timestamp')
    
    def create(self, request, *args, **kwargs):
        user = User.objects.all().first()
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(user=user)
        return Response(
            {"success": "New deadeline date has been created"},
            status=status.HTTP_201_CREATED,
            headers=self.get_success_headers(serializer.data)
            )
    
    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        return Response(
            {"success": "deadline date updated"},
            status=status.HTTP_200_OK,
            headers=self.get_success_headers(serializer.data)
        )
