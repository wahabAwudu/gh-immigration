from django.contrib import admin

from .models import DeadlineDate


class DeadlineDateAdmin(admin.ModelAdmin):
    model = DeadlineDate
    list_display = ['__str__', 'timestamp']

admin.site.register(DeadlineDate, DeadlineDateAdmin)
