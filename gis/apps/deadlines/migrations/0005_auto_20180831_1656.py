# -*- coding: utf-8 -*-
# Generated by Django 1.11.8 on 2018-08-31 16:56
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('deadlines', '0004_auto_20180828_1104'),
    ]

    operations = [
        migrations.AlterField(
            model_name='deadlinedate',
            name='timestamp',
            field=models.DateTimeField(auto_now_add=True),
        ),
    ]
