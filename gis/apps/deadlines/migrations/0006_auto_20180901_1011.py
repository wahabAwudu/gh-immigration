# -*- coding: utf-8 -*-
# Generated by Django 1.11.8 on 2018-09-01 10:11
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('deadlines', '0005_auto_20180831_1656'),
    ]

    operations = [
        migrations.AlterField(
            model_name='deadlinedate',
            name='user',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
    ]
