# -*- coding: utf-8 -*-
# Generated by Django 1.11.8 on 2018-08-28 10:42
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('deadlines', '0002_deadlinedate_user'),
    ]

    operations = [
        migrations.AlterField(
            model_name='deadlinedate',
            name='timestamp',
            field=models.DateTimeField(auto_now=True),
        ),
    ]
