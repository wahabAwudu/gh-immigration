from django.conf.urls import url
from .views import (
    DeadlineCreateView,
    DeadlineDeleteView,
    DeadlineListView,
    DeadlineUpdateView,
)

urlpatterns = [
    url(r'^$', DeadlineListView.as_view(), name="list"),
    url(r'^add-new/$', DeadlineCreateView.as_view(), name="create"),
    url(r'^(?P<pk>\d+)/update/$', DeadlineUpdateView.as_view(), name="update"),
    url(r'^(?P<pk>\d+)/delete/$', DeadlineDeleteView.as_view(), name="delete")
]
