from django.db import models


class DeadlineDate(models.Model):
    user = models.ForeignKey('auth.User', default=1, on_delete=models.CASCADE)
    deadline = models.DateField()
    timestamp = models.DateTimeField(auto_now=False, auto_now_add=True)

    def __str__(self):
        return str(self.deadline)
