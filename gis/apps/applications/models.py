from django.db import models
from django.conf import settings
from django.core.urlresolvers import reverse

from .validators import validate_file_extension


def upload_location(instance, filename):
    return "%s/%s" % (instance.user.username, filename)


class Application(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, default=0, on_delete=models.CASCADE)
    image_width = models.IntegerField(default=0)
    image_height = models.IntegerField(default=0)
    image = models.ImageField(width_field='image_width', height_field='image_height', upload_to=upload_location)

    email = models.EmailField()
    first_name = models.CharField(max_length=60)
    last_name = models.CharField(max_length=60)
    other_names = models.CharField(max_length=60, null=True, blank=True)
    gender_choices = (
        ('M', 'Male'),
        ('F', 'Female'),
    )
    gender = models.CharField(max_length=1, choices=gender_choices)
    date_of_birth = models.DateField(auto_now=False, auto_now_add=False)
    nationality = models.CharField(max_length=60)
    phone = models.CharField(max_length=15)
    address = models.TextField(max_length=200)
    city = models.CharField(max_length=60)
    country = models.CharField(max_length=60)
    birth_certificate = models.FileField(upload_to=upload_location, validators=[validate_file_extension])

    # Education Status
    QUALIFICATION_OPTIONS = (
        ('CERT', 'SSCE/WASSCE'),
        ('DIP', 'Diploma'),
        ('HND', 'Higher National Diploma (HND)'),
        ('DEG', 'Degree'),
        ('MAST', 'Masters Degree'),
        ('OTH', 'Other')
    )
    highest_qualification = models.CharField(max_length=4, choices=QUALIFICATION_OPTIONS)
    specify_qualification = models.CharField(max_length=120, null=True, blank=True)
    name_of_institution = models.CharField(max_length=250)
    course_studied = models.CharField(max_length=200)
    date_completed = models.DateField(auto_now=False, auto_now_add=False)
    educational_certificate = models.FileField(upload_to=upload_location, validators=[validate_file_extension])
    certificate_number = models.CharField(max_length=250)

    # Parent or Guadian Info
    guardian_full_name = models.CharField(max_length=250)
    guardian_phone = models.CharField(max_length=15)
    guardian_address = models.TextField(max_length=200)
    guardian_email = models.EmailField()
    relationship_to_guardian = models.CharField(max_length=50)

    # access count for admin management
    access_count = models.IntegerField(default=0)
    applied_at = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated_at = models.DateTimeField(auto_now=True, auto_now_add=False)

    # checkfield options
    ts_and_cs = models.BooleanField(default=False)

    # user's serial number
    serial_number = models.CharField(max_length=25, unique=True)

    def __str__(self):
        return self.last_name

    def get_absolute_url(self):
        return reverse('apply:details', kwargs={'pk': self.pk})
