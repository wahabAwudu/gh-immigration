from django.contrib import admin

# app models and forms
from .models import Application


class ApplicationAdmin(admin.ModelAdmin):
    list_display = ['__str__', 'first_name', 'serial_number', 'access_count']
    model = Application


admin.site.register(Application, ApplicationAdmin)
