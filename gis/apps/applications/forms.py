from django import forms

#model imports
from .models import Application


class ApplicationForm(forms.ModelForm):
    date_of_birth = forms.DateField(widget=forms.SelectDateWidget(years=[
        '1991',
        '1992',
        '1993',
        '1994',
        '1995',
        '1996',
        '1997',
        '1998',
        '1999',
        '2000'
    ]))
    date_completed = forms.DateField(widget=forms.SelectDateWidget(years=[
        '2000',
        '2001',
        '2002',
        '2003',
        '2004',
        '2005',
        '2006',
        '2007',
        '2008',
        '2009',
        '2010',
        '2011',
        '2012',
        '2013',
        '2014',
        '2015',
        '2016',
        '2017',
        '2018'
    ]))
    other_names = forms.CharField(required=False)
    specify_qualification = forms.CharField(
        label="if 'Other', specify",
        required=False,
        widget=forms.TextInput(attrs={'placeholder': 'if you chose Other above, specify'})
        )
    birth_certificate = forms.FileField(label='Birth Certificate (.pdf)')
    educational_certificate = forms.FileField(label='Educational Certificate (.pdf)')
    access_count = forms.IntegerField(required=False, widget=forms.HiddenInput())
    ts_and_cs = forms.BooleanField(required=True, label='Accept Terms and Conditions')

    class Meta:
        model = Application
        fields = [
            'image',
            'last_name',
            'first_name',
            'other_names',
            'gender',
            'date_of_birth',
            'nationality',
            'birth_certificate',
            'email',
            'phone',
            'address',
            'city',
            'highest_qualification',
            'specify_qualification',
            'name_of_institution',
            'course_studied',
            'date_completed',
            'educational_certificate',
            'certificate_number',
            'guardian_full_name',
            'guardian_phone',
            'guardian_address',
            'guardian_email',
            'relationship_to_guardian',
            'ts_and_cs',
            'access_count'
        ]


class UserSerialNumberForm(forms.ModelForm):
    class Meta:
        fields = ['serial_number']
        model = Application
