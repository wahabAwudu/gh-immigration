from django.conf.urls import url
from .views import (
    ProfileView,
    ApplicationListView,
    AllListView,
    CertListView,
    DiplomaListView,
    HNDListView,
    DegreeListView,
    MastersListView,
    OthersListView,
    SummaryView,
    ProfileUpdateView,
)

urlpatterns = [
    url(r'^$', ApplicationListView.as_view(), name='list'),
    url(r'^dashboard/application-list/all/$', AllListView.as_view(), name='all_list'),
    url(r'^dashboard/application-list/certificate/$', CertListView.as_view(), name='cert_list'),
    url(r'^dashboard/application-list/diploma/$', DiplomaListView.as_view(), name='diploma_list'),
    url(r'^dashboard/application-list/hnd/$', HNDListView.as_view(), name='hnd_list'),
    url(r'^dashboard/application-list/degree/$', DegreeListView.as_view(), name='degree_list'),
    url(r'^dashboard/application-list/masters/$', MastersListView.as_view(), name='masters_list'),
    url(r'^dashboard/application-list/others/$', OthersListView.as_view(), name='others_list'),
    url(r'^profile/(?P<pk>\d+)/$', ProfileView.as_view(), name='details'),
    url(r'^profile/(?P<pk>\d+)/summary/$', SummaryView.as_view(), name='summary'),
    url(r'^(?P<pk>\d+)/update/$', ProfileUpdateView.as_view(), name='update'),
]
