from rest_framework.viewsets import ModelViewSet
from rest_framework.permissions import IsAuthenticated, IsAdminUser, AllowAny
from rest_framework.response import Response
from rest_framework import status
from django.template.loader import get_template
from django.template.context import Context

from .serializers import ApplicationAPISerializer
from gis.apps.applications.models import Application


class ApplicationAPIViewset(ModelViewSet):
    model = Application
    serializer_class = ApplicationAPISerializer
    permission_classes = [AllowAny]

    def get_queryset(self):
        return Application.objects.all().filter(user=self.request.user)

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        serializer.save(access_count=instance.access_count+1)
        return Response(
            {"select": "Application Successfully Updated"},
            status=status.HTTP_200_OK,
            headers=self.get_success_headers(serializer.data)
        )
