from rest_framework.serializers import (
    ModelSerializer,
    PrimaryKeyRelatedField,
    HyperlinkedIdentityField,
    SerializerMethodField,
    CurrentUserDefault,
)

from gis.apps.applications.models import Application


class ApplicationAPISerializer(ModelSerializer):
    user = PrimaryKeyRelatedField(read_only=True, default=CurrentUserDefault())
    detail = HyperlinkedIdentityField(
        view_name='api:application-detail',
        lookup_field='pk'
    )

    class Meta:
        model = Application
        fields = [
            'user',
            'detail',
            'image',
            'last_name',
            'first_name',
            'other_names',
            'gender',
            'date_of_birth',
            'nationality',
            'birth_certificate',
            'email',
            'phone',
            'address',
            'city',
            'highest_qualification',
            'specify_qualification',
            'name_of_institution',
            'course_studied',
            'date_completed',
            'educational_certificate',
            'certificate_number',
            'guardian_full_name',
            'guardian_phone',
            'guardian_address',
            'guardian_email',
            'relationship_to_guardian',
            'serial_number',
            'ts_and_cs',
            'access_count'
        ]
