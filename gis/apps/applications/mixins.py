from django.shortcuts import redirect, HttpResponse

from .models import Application


class ListRedirectsMixin(object):

    def dispatch(self, request, *args, **kwargs):
        user_application = Application.objects.filter(user=request.user)
        # if user following the link is not registered or logged in, the show a 404 error
        if not request.user.is_authenticated():
            return redirect('auth_login')

        elif not user_application.exists():
            return redirect('serials:serial_link')

        return super(ListRedirectsMixin, self).dispatch(request, *args, **kwargs)


class ProfileRedirectsMixin(object):

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated():
            return redirect('auth_login')
        elif not Application.objects.all().filter(user=request.user):
            return redirect('serials:serial_link')
        return super(ProfileRedirectsMixin, self).dispatch(request, *args, **kwargs)


class ProfileUpdateRedirectsMixin(object):

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated():
            return redirect('auth_login')

        elif not Application.objects.all().filter(user=request.user):
            return redirect('serials:serial_link')

        elif Application.objects.filter(user=request.user, access_count=3):
            return HttpResponse("<h1>Your Page Edit Access is Expired.</h1>")

        return super(ProfileUpdateRedirectsMixin, self).dispatch(request, *args, **kwargs)


