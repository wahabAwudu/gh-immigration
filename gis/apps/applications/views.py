from django.shortcuts import render, get_object_or_404, redirect
from django.http import Http404, HttpResponseRedirect, HttpResponse
from django.conf import settings
from django.core.urlresolvers import reverse, reverse_lazy
from datetime import date
from django.views.generic import ListView, DetailView, UpdateView
from braces.views import StaffuserRequiredMixin
from .mixins import ProfileUpdateRedirectsMixin, ProfileRedirectsMixin, ListRedirectsMixin

# models and forms import
from .models import Application
from .forms import ApplicationForm
from gis.apps.deadlines.models import DeadlineDate


class AllListView(StaffuserRequiredMixin, ListView):
    model = Application
    template_name = 'apply/all_list.html'

    def get_context_data(self, **kwargs):
        context = super(AllListView, self).get_context_data(**kwargs)
        context['body_title'] = "LISTS OF <b>ALL</b> APPLICANTS WITH VARIOUS QUALIFICATIONS"
        return context


class CertListView(StaffuserRequiredMixin, ListView):
    model = Application
    template_name = 'apply/all_list.html'

    def get_queryset(self):
        return Application.objects.all().filter(highest_qualification='CERT')

    def get_context_data(self, **kwargs):
        context = super(CertListView, self).get_context_data(**kwargs)
        context['body_title'] = "LISTS OF ALL APPLICANTS WITH <b>SSCE/WASSCE</b> QUALIFICATION"
        return context


class DiplomaListView(StaffuserRequiredMixin, ListView):
    model = Application
    template_name = 'apply/all_list.html'

    def get_queryset(self):
        return Application.objects.all().filter(highest_qualification='DIP')

    def get_context_data(self, **kwargs):
        context = super(DiplomaListView, self).get_context_data(**kwargs)
        context['body_title'] = "LISTS OF ALL APPLICANTS WITH <b>DIPLOMA</b> QUALIFICATION"
        return context


class HNDListView(StaffuserRequiredMixin, ListView):
    model = Application
    template_name = 'apply/all_list.html'

    def get_queryset(self):
        return Application.objects.all().filter(highest_qualification='HND')

    def get_context_data(self, **kwargs):
        context = super(HNDListView, self).get_context_data(**kwargs)
        context['body_title'] = "LISTS OF ALL APPLICANTS WITH <b>Higher National Diploma (HND)</b> QUALIFICATION"
        return context


class DegreeListView(StaffuserRequiredMixin, ListView):
    model = Application
    template_name = 'apply/all_list.html'

    def get_queryset(self):
        return Application.objects.all().filter(highest_qualification='DEG')

    def get_context_data(self, **kwargs):
        context = super(DegreeListView, self).get_context_data(**kwargs)
        context['body_title'] = "LISTS OF ALL APPLICANTS WITH <b>DEGREE</b> QUALIFICATION"
        return context


class MastersListView(StaffuserRequiredMixin, ListView):
    model = Application
    template_name = 'apply/all_list.html'

    def get_queryset(self):
        return Application.objects.all().filter(highest_qualification='MAST')

    def get_context_data(self, **kwargs):
        context = super(MastersListView, self).get_context_data(**kwargs)
        context['body_title'] = "LISTS OF ALL APPLICANTS WITH <b>MASTERS DEGREE</b> QUALIFICATION"
        return context


class OthersListView(StaffuserRequiredMixin, ListView):
    model = Application
    template_name = 'apply/all_list.html'

    def get_queryset(self):
        return Application.objects.all().filter(highest_qualification='OTH')

    def get_context_data(self, **kwargs):
        context = super(OthersListView, self).get_context_data(**kwargs)
        context['body_title'] = "LISTS OF ALL APPLICANTS WITH <b>Specified OR Other</b> QUALIFICATIONS"
        return context


# lists all the applications submitted so far for a superuser. and own application for a none superuser
class ApplicationListView(ListRedirectsMixin, ListView):
    model = Application
    template_name = 'apply/index.html'

    def get_queryset(self):
        if self.request.user.is_staff or self.request.user.is_superuser:
            return Application.objects.all().order_by('-applied_at')
        else:
            return Application.objects.all().filter(user=self.request.user)


# shows the profile summary of the applicant
class ProfileView(ProfileRedirectsMixin, DetailView):
    model = Application
    template_name = 'apply/profile.html'

    def get_context_data(self, **kwargs):
        # instance = get_object_or_404(Application, pk=self.object.pk)
        context = super(ProfileView, self).get_context_data(**kwargs)
        context['visits_left'] = 3-self.object.access_count
        return context


# updates the applicationn information of the user
class ProfileUpdateView(ProfileUpdateRedirectsMixin, UpdateView):
    model = Application
    template_name = 'apply/update.html'
    form_class = ApplicationForm

    def get_success_url(self):
        return reverse_lazy('apply:details', kwargs={'pk': self.object.pk})

    def get_context_data(self, **kwargs):
        context = super(ProfileUpdateView, self).get_context_data(**kwargs)
        context['todays_date'] = date.today()
        context['deadline'] = DeadlineDate.objects.all().latest('timestamp')
        return context

    def form_valid(self, form):
        form.instance.access_count += 1
        return super(ProfileUpdateView, self).form_valid(form)


class SummaryView(DetailView):
    model = Application
    template_name = 'apply/summary.html'
