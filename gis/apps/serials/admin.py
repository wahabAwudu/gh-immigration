from django.contrib import admin
from .models import Serials
from .forms import SerialsGenerateForm


class SerialsAdmin(admin.ModelAdmin):
    list_display = ['__str__', 'status', 'created_at', 'updated_at']
    form = SerialsGenerateForm


admin.site.register(Serials, SerialsAdmin)
