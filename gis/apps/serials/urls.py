from django.conf.urls import url
from .views import (
    serials_view,
    SerialsCreateView,
    SerialsUpdateView,
    ActiveSerialsListView,
    AllSerialsListView,
    InactiveSerialsListView,
    SerialsDeleteView,
)

urlpatterns = [
    url(r'^$', serials_view, name="serial_link"),
    url(r'^all/$', AllSerialsListView.as_view(), name="list"),
    url(r'^active/$', ActiveSerialsListView.as_view(), name="active-list"),
    url(r'^inactive/$', InactiveSerialsListView.as_view(), name="inactive-list"),
    url(r'^generate/$', SerialsCreateView.as_view(), name="create"),
    url(r'^(?P<pk>\d+)/update/$', SerialsUpdateView.as_view(), name="update"),
    url(r'^(?P<pk>\d+)/delete/$', SerialsDeleteView.as_view(), name="delete")
]
