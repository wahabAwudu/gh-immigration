from rest_framework.serializers import (
    ModelSerializer,
    PrimaryKeyRelatedField,
    CurrentUserDefault,
    SerializerMethodField,
    HyperlinkedIdentityField,
)
from gis.apps.serials.models import Serials


class SerialNumberAPISerializer(ModelSerializer):
    user = PrimaryKeyRelatedField(read_only=True, default=CurrentUserDefault())
    id = SerializerMethodField()
    # detail = HyperlinkedIdentityField(
    #     view_name='api:serial_number-detail'
    # )
    
    class Meta:
        model = Serials
        fields = [
            'id',
            'user',
            'serial_number',
            'status',
            'updated_at'
        ]
    
    def get_id(self, obj):
        return obj.id
