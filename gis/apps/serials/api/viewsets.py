from rest_framework.viewsets import ModelViewSet
from rest_framework.permissions import IsAdminUser, AllowAny
from rest_framework.response import Response
from rest_framework import status
from .serializers import SerialNumberAPISerializer
from gis.apps.serials.models import Serials

from django.contrib.auth.models import User
from django.core.mail import EmailMessage
from django.template.loader import get_template
from django.template import Context
from django.template.loader import render_to_string
from django.shortcuts import render

import string
import random


class SerialNumberAPIViewset(ModelViewSet):
    model = Serials
    serializer_class = SerialNumberAPISerializer
    permission_classes = [AllowAny]

    def get_queryset(self):
        return Serials.objects.all().order_by('-updated_at')

    def create(self, request, *args, **kwargs):
        user = User.objects.all().first()
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(user=user)

        # send an email to the user after saving the object
        current_user = User.objects.all().first()
        user_email = current_user.email
        subject = "Account Activate"
        # from_email = "metesteremail@gmail.com"

        ctx = {
            'activity_name': 'Deposite Success!',
            'user': current_user.username,
            'user_email': current_user.email,
            'subject': subject,
            'main_button_text': 'View Orders',
            'main_button_link': "{% url 'deposit_list' %}",
            'button_needed': True,
            'amount': '$1000',
        }

        msg = EmailMessage()
        message = render_to_string('emails/deposit_success.html',ctx)
        msg.subject = subject
        msg.body = message
        msg.content_subtype = 'html'
        msg.to = [user_email]
        # msg.from_email = 'metesteremail@gmail.com'
        msg.send(fail_silently=False)
        return Response(
            serializer.data,
            status=status.HTTP_201_CREATED,
            headers=self.get_success_headers(serializer.data)
            )

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        return Response(
            serializer.data,
            status=status.HTTP_200_OK,
            headers=self.get_success_headers(serializer.data)
        )
