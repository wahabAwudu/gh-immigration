from django.db import models
from django.conf import settings
import random
import string

class Serials(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    serial_number = models.CharField(max_length=25, blank=True, null=True)
    STATUS_OPTIONS = (
        ("ACT", "ACTIVE"),
        ("IN", "INACTIVE")
    )
    status = models.CharField(max_length=3, choices=STATUS_OPTIONS, default="ACT")
    created_at = models.DateTimeField(auto_now=False, auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True, auto_now_add=False)

    def __str__(self):
        return str(self.serial_number)

    def get_serial_number(self):
        return ''.join(random.choice(string.ascii_uppercase + string.ascii_lowercase + string.digits) for _ in range(25))

    def save(self, *args, **kwargs):
        if not self.serial_number:
            self.serial_number = self.get_serial_number()
        super().save()
