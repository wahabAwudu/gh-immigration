from django.apps import AppConfig


class SerialsConfig(AppConfig):
    name = 'apps.serials'
