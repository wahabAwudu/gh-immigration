from django import forms
from .models import Serials


class SerialNumberForm(forms.ModelForm):
    serial_number = forms.CharField(widget=forms.TextInput(attrs={'autocomplete': 'off'}), max_length=25)

    class Meta:
        fields = ['serial_number']
        model = Serials


class SerialsGenerateForm(forms.ModelForm):
    class Meta:
        model = Serials
        fields = ['user', 'status']


class SerialsUpdateForm(forms.ModelForm):
    serial_number = forms.CharField(max_length=25, widget=forms.TextInput({"autocomplete":"off"}), required=True)

    class Meta:
        model = Serials
        fields = ['serial_number', 'status']
