from django.shortcuts import render, redirect, get_object_or_404
from django.http import Http404, HttpResponse, HttpResponseRedirect
from django.core.urlresolvers import reverse_lazy
from django.views.generic import UpdateView, CreateView, ListView, DeleteView
from datetime import date

# from this App (i.e the Serials App)
from .models import Serials
from .forms import SerialNumberForm, SerialsUpdateForm, SerialsGenerateForm

# from Another App (i.e Application App)
from gis.apps.applications.models import Application

from braces.views import LoginRequiredMixin, StaffuserRequiredMixin, SuperuserRequiredMixin


def serials_view(request):
    # redirects to login page if not logged in
    if not request.user.is_authenticated():
        return redirect('auth_login')
    form = SerialNumberForm(request.POST or None)
    if form.is_valid():
        instance = form.save(commit=False)
        # checking if d serial is valid and no user has it in use.
        if Serials.objects.all().filter(serial_number=instance.serial_number) and not Application.objects.all().filter(serial_number=instance.serial_number):
            # creates a new application for the use if the serial number is not wrong or not in use
            Application.objects.create(
                user=request.user,
                serial_number=instance.serial_number,
                date_of_birth = date.today(),
                date_completed = date.today()
            )
            return redirect('apply:list')
        # checks to see if we already created the applicaton for the current user
        elif Application.objects.all().filter(user=request.user, serial_number=instance.serial_number):
            return redirect('apply:list')
        # checks to prevent multiple use of serial number
        elif Serials.objects.filter(serial_number=instance.serial_number) and Application.objects.all().filter(serial_number=instance.serial_number):
            return HttpResponse("<h1> OOPS!! Serial Number is In Use</h1>")
        # checks to prevent the use of invalid serial number
        elif not Serials.objects.all().filter(serial_number=instance.serial_number):
            return HttpResponse("<h1> OOPSS!! Wrong Serial Number,Try Again</h1>")

    context = {
        'form': form,
    }

    return render(request, 'serials/index.html', context)


class AllSerialsListView(LoginRequiredMixin, StaffuserRequiredMixin, ListView):
    model = Serials
    queryset = Serials.objects.all().order_by('-updated_at')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["page_header"] = "List of All Serial Numbers"
        return context


class ActiveSerialsListView(LoginRequiredMixin, StaffuserRequiredMixin, ListView):
    model = Serials
    queryset = Serials.objects.filter(status="ACT").order_by('-updated_at')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["page_header"] = "List of ACTIVE Serial Numbers"
        return context


class InactiveSerialsListView(LoginRequiredMixin, StaffuserRequiredMixin, ListView):
    model = Serials
    queryset = Serials.objects.filter(status="IN").order_by('-updated_at')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["page_header"] = "List of INACTIVE Serial Numbers"
        return context


class SerialsCreateView(LoginRequiredMixin, StaffuserRequiredMixin, CreateView):
    model = Serials
    fields = ['status']
    success_url = reverse_lazy("serials:list")

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super(SerialsCreateView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["page_title"] = "Generate Serial" 
        context["page_header"] = "Generate A Serial Number"
        context["button_text"] = "Generate"
        return context
    

class SerialsUpdateView(LoginRequiredMixin, StaffuserRequiredMixin, UpdateView):
    model = Serials
    form_class = SerialsUpdateForm
    success_url = reverse_lazy("serials:list")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["page_title"] = "Update Serial" 
        context["page_header"] = "Update Serial Number"
        context["button_text"] = "Update"
        return context


class SerialsDeleteView(LoginRequiredMixin, SuperuserRequiredMixin,DeleteView):
    model = Serials
    success_url = reverse_lazy("serials:list")
